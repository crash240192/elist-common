﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EList.Localization
{
    public enum Languages : int
    { 
        english = 0,
        russian = 1
    }
}
