﻿namespace EList.Common.Constants
{
    public static class Constants
    {
        public static class HttpHeaderKeys
        {
            public const string CORRELATION_ID = "correlationId";
        }

        public const int INTERNAL_ERROR_CODE = -1;
    }
}
