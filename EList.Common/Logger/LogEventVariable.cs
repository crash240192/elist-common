﻿namespace EList.Common.Logger
{
    public enum LogEventVariable
    {
        CorrelationId,
        Guid,
        LoggerName,
        LogLevel,
        ClientIp,
        IncommingMessage,
        Message,
        StartEventTime,
        EndEventTime,
        RequestId
    }
}
