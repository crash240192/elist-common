﻿namespace EList.Common.Logger.Containers
{
    public sealed class RequestIdContainer
    {
        public string RequestId { get; set; }
    }
}
