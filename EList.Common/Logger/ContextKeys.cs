﻿namespace EList.Common.Logger
{
    public static class ContextKeys
    {

        public const string CORRELATION_ID = "correlationId";
        public const string REQUEST = "request";
        public const string START_DATE = "startDate";
        public const string LOGGER_NAME = "logger";

    }
}
