﻿namespace EList.Common.DI
{
    public interface IServiceMappingProvider
    {
        ServiceMapping GetServiceMapping();
    }
}
