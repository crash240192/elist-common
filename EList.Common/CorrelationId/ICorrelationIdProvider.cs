﻿namespace EList.Common.CorrelationId
{
    public interface ICorrelationIdProvider
    {
        string Get();
    }
}
