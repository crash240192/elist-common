﻿namespace EList.Common.DI
{
    public class ServiceMappingEntry
    {
        public Type Service { get; set; }
        public Type Implementation { get; set; }
    }
}
