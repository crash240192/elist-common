﻿namespace EList.Common.HttpRestClient
{
    public class HttpResponse
    {
        public string Body { get; set; }

        public int StatusCode { get; set; }
    }
}
